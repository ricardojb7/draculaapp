﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Dracula.APP {
    public partial class MainPage : ContentPage {
        public MainPage()
        {
            InitializeComponent();

        }

        private async void BotaoCadastroUsuario(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new CadastroUsuario());
        }

        private async void BotaoLogin(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new PaginaMestre());
        }
    }
}
