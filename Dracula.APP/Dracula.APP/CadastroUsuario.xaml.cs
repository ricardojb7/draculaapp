﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Dracula.APP
{
	public partial class CadastroUsuario : ContentPage
	{
		public CadastroUsuario ()
		{
			InitializeComponent ();
		}
        private async void BotaoCadastroUsuario(object sender, EventArgs e)
        {
            await  Navigation.PushModalAsync(new PaginaMestre());
        }
    }
}