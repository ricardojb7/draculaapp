﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Dracula.APP
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaMestreMaster : ContentPage
    {
        public ListView ListView;

        public PaginaMestreMaster()
        {
            InitializeComponent();

            BindingContext = new PaginaMestreMasterViewModel();
            ListView = MenuItemsListView;
        }

        class PaginaMestreMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<PaginaMestreMenuItem> MenuItems { get; set; }
            
            public PaginaMestreMasterViewModel()
            {
                MenuItems = new ObservableCollection<PaginaMestreMenuItem>(new[]
                {
                    new PaginaMestreMenuItem { Id = 0, Title = "Page 1" },
                    new PaginaMestreMenuItem { Id = 1, Title = "Page 2" },
                    new PaginaMestreMenuItem { Id = 2, Title = "Page 3" },
                    new PaginaMestreMenuItem { Id = 3, Title = "Page 4" },
                    new PaginaMestreMenuItem { Id = 4, Title = "Page 5" },
                });
            }
            
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}